#! /bin/sh -x

# test for a bug in sh(1)
#
# normally, interrupting this script should send a signal to the
# subshell and the latter should call the trap, but doesn't.

# cleanup function
cleanup () {
    echo "cleaning up $pid"
    kill $pid
}

# this is used to send a signal to the subshell
cleanup_subshell () {
    echo "cleaning up subshell $subshell"
    kill $subshell
}

# fork a subshell
(
    echo "in subshell"
    # launch an arbitrary command that will take a while
    tail -f /etc/motd &
    pid=$!
    # this is the bug: this command has no effect
    trap cleanup 1 2 15
    wait $pid || echo "command failed"
) &

# make sure killing this script also kills the subshell
subshell=$!
trap cleanup_subshell 1 2 15

wait
