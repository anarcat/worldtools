PROJECT=worldtools

SCRIPTS=upgrade buildit whereintheworld

MAN=upgrade.8 buildit.8 whereintheworld.8

SCRIPTSDIR?=${PREFIX}/sbin
SHAREDIR?=${PREFIX}/share

RELEASE=1.3.1
REL_TAG=WORLDTOOLS_V_${RELEASE:S/./_/g}

PREFIX?=/usr

all: ${PROJECT}

${PROJECT}:

release: ${PROJECT}-${RELEASE}.tar.gz

${PROJECT}-${RELEASE}.tar.gz: clean ${PROJECT}-${RELEASE} ChangeLog
	@echo "Building release tarball"
	@tar -v -c -z --exclude CVS -f $@ ${PROJECT}-${RELEASE}
	@md5 $@

${PROJECT}-${RELEASE}: ChangeLog
	@echo "Checking out temp copy of release ${REL_TAG}"
	@echo "If you forgot to tag this release, use the tag target"
	@cvs co -d $@ -r${REL_TAG} ${PROJECT} > /dev/null
	@cp ChangeLog $@

tag:
	@echo "Tagging ${REL_TAG}"
	@cvs tag ${REL_TAG} || true

ChangeLog::
	@cvs2cl -r -b -t --no-wrap -S 2> /dev/null

.include <bsd.prog.mk>
